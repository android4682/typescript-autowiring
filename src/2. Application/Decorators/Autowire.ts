import "reflect-metadata"
import { METADATA_AUTOWIRE } from "../../1. Domain/Constants";

export function AutoWire(): Function
{
  return (target: object, propertyKey: string | symbol | undefined, index: number) => {
    if (propertyKey) {
      Reflect.defineMetadata(METADATA_AUTOWIRE, true, target, propertyKey);
    } else {
      Reflect.defineMetadata(METADATA_AUTOWIRE, true, target);
    }
  }
}
