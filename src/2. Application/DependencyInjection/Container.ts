import "reflect-metadata";
import { InifinityObject, SimpleKeyStringObject, SpecialMap, isClass, isDeepInstanceOf } from "@android4682/typescript-toolkit";
import { MissingService } from "../Errors/MissingService";
import { ClassObject } from "../../1. Domain/Interfaces/ClassObject";
import { BetterConsole, LogLevel, getMessageAndStack } from "@android4682/better-console";
import { FailedToAutowire } from "../Errors/FailedToAutowire";
import { METADATA_AUTOWIRE } from "../../1. Domain/Constants";
import { MissingAutowireServices } from "../Errors/MissingAutowireServices";
import { TooManyResults } from "../Errors/TooManyResults";

export interface RegisterServiceOptions
{
  ignoreMissingParamters: boolean
  overwrite: boolean
  log: boolean
  retryFailed: boolean
}

export interface RegisterServiceOptionalOptions
{
  ignoreMissingParamters?: boolean
  overwrite?: boolean
  log?: boolean
  retryFailed?: boolean
}

export interface CategorizedUnregisteredService
{
  service: ClassObject<any> | object
  category?: string
  metadata?: SimpleKeyStringObject<any>
}

export interface CategorizedService<S = object>
{
  service: S
  category?: string
  metadata?: SimpleKeyStringObject<any>
}

export type ServiceFilterFunction = (service: CategorizedService, serviceName: string) => boolean

export interface ServiceFilter
{
  singleResult?: boolean
  throwTantrumWhenMissing?: boolean
  throwTantrumWhenHavingMultipleResultsButAskedForOne?: boolean
}

export interface FilledServiceFilter
{
  throwTantrumWhenMissing: boolean
}

export class Container
{
  protected services: SpecialMap<string, CategorizedService> = new SpecialMap<string, CategorizedService>()
  protected failedAutowiredServices: SpecialMap<string, CategorizedService> = new SpecialMap<string, CategorizedService>()
  protected logger: BetterConsole;

  public constructor(logger: BetterConsole, services: SpecialMap<string, CategorizedService> = new SpecialMap<string, CategorizedService>())
  {
    this.logger = logger
    services.set(this.constructor.name, {
      service: this
    })
    this.services = services
    this.constructServices(this.services)
  }

  private _getOption(option: typeof defaults | undefined, defaults: any): typeof defaults
  {
    return (option !== undefined) ? option : defaults
  }

  /**
   * @throws { MissingService }
   */
  public get<T = object>(serviceName: string, throwTantrumWhenMissing: boolean = true): CategorizedService<T> | false
  {
    let service = this.services.get(serviceName)

    if (! service) {
      if (throwTantrumWhenMissing) throw new MissingService(serviceName);
      else return false
    }

    return <CategorizedService<T>> service
  }

  private _fillServiceFilter(options: ServiceFilter): FilledServiceFilter
  {
    return {
      ...options,
      throwTantrumWhenMissing: this._getOption(options.throwTantrumWhenMissing, true)
    }
  }

  private _returnLogicGetByFilter(results: SpecialMap<string, CategorizedService>, filter: FilledServiceFilter): SpecialMap<string, CategorizedService>
  {
    if (filter.throwTantrumWhenMissing && results.size === 0) throw new MissingService()

    return results
  }

  public getByFilter(filter: ServiceFilterFunction, option: ServiceFilter = {}): SpecialMap<string, CategorizedService>
  {
    const filledFilter = this._fillServiceFilter(option)

    const results: SpecialMap<string, CategorizedService> = new SpecialMap<string, CategorizedService>()
    this.services.forEach((service, serviceName) => {
      if (filter(service, serviceName)) results.set(serviceName, service) 
    })

    return this._returnLogicGetByFilter(results, filledFilter)
  }

  public retryFailedAutowiredServices(lastRegisteredServiceName?: string): void
  {
    let retry: boolean = false

    this.failedAutowiredServices.forEach((service, serviceName) => {
      const isRegistered = this.registerCategorizedService(service, serviceName, {
        overwrite: true,
        log: false,
        retryFailed: false
      })

      if (isRegistered) {
        this.logger.success(`Successfully re-registered '${serviceName}'${(lastRegisteredServiceName) ? ` because of '${lastRegisteredServiceName}' is finally available` : ''}.`)

        lastRegisteredServiceName = serviceName
        this.failedAutowiredServices.delete(serviceName)
        retry = true
      } else {
        this.logger.debug(`Service '${serviceName}' still unavailable.`)
      }
    })

    if (retry) this.retryFailedAutowiredServices(lastRegisteredServiceName)
  }

  private _fillRegisterServiceOptions(options: RegisterServiceOptionalOptions): RegisterServiceOptions
  {
    return {
      ignoreMissingParamters: this._getOption(options.ignoreMissingParamters, false),
      overwrite: this._getOption(options.overwrite, false),
      log: this._getOption(options.log, true),
      retryFailed: this._getOption(options.retryFailed, true),
    }
  }

  /**
   * @deprecated Use registerCategorizedService instead. Services registered through this method will be category-less and will have no metadata.
   */
  public registerNewService(service: ClassObject<any> | InifinityObject, serviceName: string, options: RegisterServiceOptionalOptions = {}): boolean
  {
    return this.registerCategorizedService({
      service
    }, serviceName, options)
  }

  public registerCategorizedService(service: CategorizedUnregisteredService, serviceName: string, options: RegisterServiceOptionalOptions = {}): boolean
  {
    const filledOptions = this._fillRegisterServiceOptions(options)

    if (this.services.has(serviceName)) {
      if (filledOptions.overwrite) {
        if (filledOptions.log) this.logger.warn(`Overwriting registered service '${serviceName}'...`)
      } else {
        if (filledOptions.log) this.logger.warn(`Service '${serviceName}' is already registered.`)
  
        return false;
      }
    }

    if (this.isServiceStatic(service.service)) {
      try {
        service.service = this.autoWireService(<ClassObject<any>> service.service, [], filledOptions.ignoreMissingParamters)
      } catch (e) {
        if (e instanceof MissingService) {
          if (filledOptions.log) this.logger.debug('Adding failed autowired service: ' + serviceName)
          this.failedAutowiredServices.set(serviceName, service)
        }

        if (filledOptions.log) {
          let [msg, stack] = getMessageAndStack(e)
          this.logger.error(msg)
          this.logger.info('We will try autowiring again if another service gets successfully registered in this container.')
          this.logger.dir(stack, undefined, LogLevel.debug)
        }

        return false
      }
    }

    this.services.set(serviceName, service)
    if (filledOptions.log) this.logger.success(`Successfully registered '${serviceName}' service!`)
    if (filledOptions.retryFailed) this.retryFailedAutowiredServices(serviceName)

    return true;
  }  

  public isServiceStatic(service: ClassObject<any> | InifinityObject): boolean
  {
    return (!! service.prototype && !! service.prototype.constructor)
  }

  /**
   * @throws { FailedToAutowire }
   */
  public getParameterTypes(objectPrototype: any, methodName: string): any[]
  {
    let parameters: any[] | undefined = undefined
    let isAutoWireable = false

    if (methodName === 'constructor') {
      isAutoWireable = Reflect.getMetadata(METADATA_AUTOWIRE, objectPrototype)
      parameters = Reflect.getMetadata("design:paramtypes", objectPrototype)
    } else {
      isAutoWireable = Reflect.getMetadata(METADATA_AUTOWIRE, objectPrototype, methodName)
      parameters = Reflect.getMetadata("design:paramtypes", objectPrototype, methodName);
    }
    
    if (parameters === undefined) {
      if (isAutoWireable === true) parameters = []
      else throw new FailedToAutowire(objectPrototype.name, methodName)
    }

    return parameters
  }

  private canSkipAutoWiring(arg: any, parameterType: any): boolean
  {
    return (arg !== undefined || parameterType === undefined || (arg !== undefined && isDeepInstanceOf(arg, parameterType)))
  }

  /**
   * @throws { FailedToAutowire }
   * @throws { MissingService }
   */
  public autoWireService(classObject: ClassObject<any>, args: any[] = [], ignoreMissingParamters: boolean = false): object
  {
    let parameterTypes = this.getParameterTypes(classObject, 'constructor')
    let missingParameterTypes: string[] = []

    for (let i = 0; i < parameterTypes.length; i++) {
      const parameterType = parameterTypes[i];
      
      if (this.canSkipAutoWiring(args[i], parameterType)) continue

      try {
        const wrappedService = <CategorizedService> this.get(parameterType.name)
        args[i] = wrappedService.service
      } catch (e) {
        if (! ignoreMissingParamters) throw e 
      }
      
      if (args[i] && isClass(args[i])) {
        args[i] = this.autoWireService(args[i], [], ignoreMissingParamters)
      } else if (args[i] === undefined || (typeof args[i] !== typeof parameterType && ! (args[i] instanceof parameterType))) {
        missingParameterTypes.push(parameterType.name)
      }
    }
    
    if (args.length < parameterTypes.length) throw new MissingAutowireServices(classObject.name, missingParameterTypes)

    return new classObject(...args)
  }

  public constructServices(services: SpecialMap<string, ClassObject<any> | InifinityObject>): typeof services
  {
    let initiated: string[] = []
    let lastRound: number = 0
    
    do {
      lastRound = initiated.length

      services.forEach((service, name) => {
        if (! this.isServiceStatic(service)) {
          if (initiated.indexOf(name) === -1) {
            initiated.push(name)
          }
          
          return
        }
  
        let initiatedService
        try {
          initiatedService = this.autoWireService(<ClassObject<any>> service, [], true)
        } catch (e) {
          if (e instanceof MissingAutowireServices) return
          
          throw e
        }
        
        if (! this.isServiceStatic(initiatedService)) {
          initiated.push(name)

          services.set(name, initiatedService)
        }
      })
    } while (initiated.length < services.keysAsArray().length && lastRound !== initiated.length)

    if (initiated.length < services.keysAsArray().length) {
      let missingServices: string[] = []

      for (let i = 0; i < services.keysAsArray().length; i++) {
        const serviceName = services.keysAsArray()[i];
        if (initiated.indexOf(serviceName) === -1) missingServices.push(serviceName)
      }

      this.logger.warn('The following services didn\'t get constructed, probably because of missing parameters which are missing in the dependency injector container.')
      this.logger.warn(missingServices)

      for (let i = 0; i < missingServices.length; i++) services.delete(missingServices[i])
    }

    return services
  }

  /**
   * @deprecated Use regiserCategorizedServiceList instead. Services registered through this method will be category-less and will have no metadata.
   * 
   * @returns { boolean } False if any failed, true if all succeded
   */
  public regiserServiceList(services: SpecialMap<string, InifinityObject>, ignoreMissingParamters: boolean = false, overwrite: boolean = false): boolean
  {
    let totalSuccess = true
    services.forEach((service, name) => {
      let success = this.registerCategorizedService({
        service
      }, name, {
        ignoreMissingParamters,
        overwrite
      })
      if (totalSuccess && ! success) totalSuccess = success
    })

    return totalSuccess
  }

  /**
   * @returns { boolean } False if any failed, true if all succeded
   */
  public regiserCategorizedServiceList(services: SpecialMap<string, CategorizedUnregisteredService>, options: RegisterServiceOptionalOptions = {}): boolean
  {
    let totalSuccess = true
    services.forEach((service, name) => {
      let success = this.registerCategorizedService(service, name, options)
      if (totalSuccess && ! success) totalSuccess = success
    })

    return totalSuccess
  }

  public getServicesThatAreAnInstanceOf(instance: object): SpecialMap<string, CategorizedService>
  {
    const list = new SpecialMap<string, CategorizedService>()

    this.services.forEach((categorizedService, serviceName) => {
      if (isDeepInstanceOf(categorizedService.service, instance)) {
        list.set(serviceName, categorizedService)
      }
    })

    return list
  }
}
