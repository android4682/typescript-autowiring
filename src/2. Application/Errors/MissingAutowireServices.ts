export class MissingAutowireServices extends Error {
  public errorName: string = this.constructor.name;

  public constructor(className: string, missingServices: string[] = [])
  {
    super(`Couldn't autowire '${className}'. The following services are missing in this container: ${missingServices.join(', ')}`)
  }
}
