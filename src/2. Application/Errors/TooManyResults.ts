export class TooManyResults extends Error
{
  public errorName: string = this.constructor.name;

  public constructor(expected?: number, got?: number)
  {
    super(`Too many results. ${(expected && got) ? `We expected ${expected} elements but we got ${got}.` : `We only requested a certain amount but we got more.`}`)
  }
}