import { AutoWire } from "../../src/2. Application/Decorators/Autowire";
import { DummyService } from "./DummyService";

@AutoWire()
export class DependencyDummy
{
  private _dummy: DummyService

  public constructor(dummy: DummyService)
  {
    if (! (dummy instanceof DummyService)) {
      console.dir({dummyArgInClass: dummy});
      // throw new Error('Missing dummy argument.')
    }
    this._dummy = dummy
  }

  public get dummy(): DummyService
  {
    return this._dummy
  }
}
