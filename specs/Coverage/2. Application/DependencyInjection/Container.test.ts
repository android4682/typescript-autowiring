import { shouldRunSuite } from "../../../SuiteRunChecker";
import { CategorizedService, CategorizedUnregisteredService, Container } from "../../../../src/2. Application/DependencyInjection/Container";
import { SpecialMap, randomStringGenerator } from "@android4682/typescript-toolkit";
import { MissingService } from "../../../../src/2. Application/Errors/MissingService";
import { DummyService } from "../../../Helper/DummyService";
import { StevenHe } from "../../../Helper/Failure";
import { FailedToAutowire } from "../../../../src/2. Application/Errors/FailedToAutowire";
import { ClassObject } from "../../../../src/1. Domain/Interfaces/ClassObject";
import { BetterConsole } from "@android4682/better-console";
import { DependencyDummy } from "../../../Helper/DependencyDummy";
import { AutoWireDummy } from "../../../Helper/AutoWireDummy";

if (shouldRunSuite('UNIT')) {
  let logger = new BetterConsole()
  beforeEach(() => {
    logger = new BetterConsole()
  })

  describe('Container', () => {
    it('should, when a missing service is requested, throw a tantrum by default', () => {
      let container: Container = new Container(logger)

      expect(container.get.bind(container, randomStringGenerator(12))).toThrowError(MissingService)
    })

    it('should, when a missing service is requested, NOT throw a tantrum when asked', () => {
      let container: Container = new Container(logger)

      expect(container.get(randomStringGenerator(12), false)).toBe(false)
    })

    it('isServiceStatic method should return true when given a static class', () => {
      let container: Container = new Container(logger)

      expect(container.isServiceStatic(DummyService)).toBe(true)
    })

    it('isServiceStatic method should return false when given a constructed class', () => {
      let container: Container = new Container(logger)
      let constructed = new DummyService()

      expect(container.isServiceStatic(constructed)).toBe(false)
    })

    it('should not be able to register a static service under an already registered service name', () => {
      let container: Container = new Container(logger)
      let serviceName: string = randomStringGenerator(12)

      expect(container.registerCategorizedService({ service: DummyService }, serviceName)).toBe(true)
      expect(container.registerCategorizedService({ service: DummyService }, serviceName)).toBe(false)
    })

    it('should not be able to register a constructed service under an already registered service name', () => {
      let container: Container = new Container(logger)
      let serviceName: string = randomStringGenerator(12)
      let constructedService = new DummyService()

      expect(container.registerCategorizedService({ service: constructedService }, serviceName)).toBe(true)
      expect(container.registerCategorizedService({ service: constructedService }, serviceName)).toBe(false)
    })

    it('should be able to register a constructed service under an already registered service name when asked', () => {
      let container: Container = new Container(logger)
      let serviceName: string = randomStringGenerator(12)
      let constructedService = new DummyService()

      expect(container.registerCategorizedService({ service: constructedService }, serviceName)).toBe(true)
      expect(container.registerCategorizedService({ service: constructedService }, serviceName, {
        overwrite: true
      })).toBe(true)
    })

    it('should fail to auto wire a service which requires another services that hasn\'t been registered yet', () => {
      let container: Container = new Container(logger)
      let serviceName: string = randomStringGenerator(12)

      expect(container.isServiceStatic(StevenHe)).toBe(true)
      expect(container.registerCategorizedService({ service: StevenHe }, serviceName)).toBe(false)
    })

    it('should fail getParameterTypes from a non-autowired service', () => {
      let container: Container = new Container(logger)

      expect(container.getParameterTypes.bind(container, StevenHe, 'calculator')).toThrowError(FailedToAutowire)
    })

    it('should be able to mass construct services', () => {
      let container: Container = new Container(logger)
      let services = new SpecialMap<string, ClassObject<any>>([
        ['DummyService', DummyService],
        ['AutoWireDummy', AutoWireDummy]
      ])
      
      let result_services = container.constructServices(services)
      expect(result_services.get('DummyService')).toBeInstanceOf(DummyService)
      expect(result_services.get('AutoWireDummy')).toBeInstanceOf(AutoWireDummy)
    })

    it('should fail to mass construct services if depency is missing in container', () => {
      let container: Container = new Container(logger)
      let services = new SpecialMap<string, ClassObject<any>>([
        ['DependencyDummy', DependencyDummy]
      ])
      
      let result_services = container.constructServices(services)
      expect(result_services.size).toBe(0)
    })
    
    it('should be able to return a list of services that are an instance of a given class', () => {
      let container: Container = new Container(logger)
      let services = new SpecialMap<string, CategorizedUnregisteredService>([
        ['DummyService', { service: DummyService }],
        ['DependencyDummy', { service: DependencyDummy }]
      ])
      container.regiserCategorizedServiceList(services)
      
      let foundServices = container.getServicesThatAreAnInstanceOf(DummyService)

      expect(foundServices.size).toBe(1)
    })
    
    it('should be able to get all services based on a filter', () => {
      let container: Container = new Container(logger)
      let services = new SpecialMap<string, CategorizedUnregisteredService>([
        ['DummyService', { service: DummyService }],
        ['DependencyDummy', { service: DependencyDummy }]
      ])
      container.regiserCategorizedServiceList(services)
      
      const foundService = <SpecialMap<string, CategorizedService>|null> container.getByFilter((service, serviceName) => {
        return (serviceName === 'DummyService')
      }, {
        throwTantrumWhenMissing: false
      })

      expect(foundService?.size).toBe(1)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Container', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}