import "reflect-metadata"
import { shouldRunSuite } from "../../../SuiteRunChecker";
import { AutoWireDummy } from "../../../Helper/AutoWireDummy";
import { METADATA_AUTOWIRE } from "../../../../src/1. Domain/Constants";

if (shouldRunSuite('UNIT')) {
  describe('Autowire', () => {
    it('should be able to add auto wire metadata to class constructor', () => {
      expect(Reflect.getMetadata(METADATA_AUTOWIRE, AutoWireDummy)).toBe(true)
    })

    it('should be able to add auto wire metadata to a method', () => {
      let autoWire = new AutoWireDummy()

      expect(Reflect.getMetadata(METADATA_AUTOWIRE, autoWire, 'someMethod')).toBe(true)
    })
    
    it('should be able to add auto wire metadata to a property', () => {
      let autoWire = new AutoWireDummy()

      expect(Reflect.getMetadata(METADATA_AUTOWIRE, autoWire, 'someProperty')).toBe(true)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('Autowire', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}