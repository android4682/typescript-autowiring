import { MissingAutowireServices } from "../../../../src/2. Application/Errors/MissingAutowireServices";
import { DummyService } from "../../../Helper/DummyService";
import { shouldRunSuite } from "../../../SuiteRunChecker";

if (shouldRunSuite('UNIT')) {
  describe('MissingAutowireServices', () => {
    it('should be able to be made', () => {
      let error = new MissingAutowireServices(DummyService.name)
      expect(error).toBeInstanceOf(MissingAutowireServices)
      expect(error.errorName).toBe(MissingAutowireServices.name)
      expect(error.message).toBe(`Couldn't autowire '${DummyService.name}'. The following services are missing in this container: `)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('MissingAutowireServices', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}