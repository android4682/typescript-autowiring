import { randomStringGenerator } from "@android4682/typescript-toolkit";
import { shouldRunSuite } from "../../../SuiteRunChecker";
import { MissingService } from "../../../../src/2. Application/Errors/MissingService";

if (shouldRunSuite('UNIT')) {
  describe('MissingService', () => {
    it('should be able to be made', () => {
      const serviceName = randomStringGenerator(16)
      const error = new MissingService(serviceName)
      expect(error).toBeInstanceOf(MissingService)
      expect(error.errorName).toBe(MissingService.name)
      expect(error.message).toBe(`Couldn't find service with name '${serviceName}', did you configure it?`)
    })

    it('should be able to be made without arguments', () => {
      const error = new MissingService()
      expect(error).toBeInstanceOf(MissingService)
      expect(error.errorName).toBe(MissingService.name)
      expect(error.message).toBe(`Couldn't find service, did you configure it?`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('MissingService', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}