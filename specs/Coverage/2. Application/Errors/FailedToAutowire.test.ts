import { randomStringGenerator } from "@android4682/typescript-toolkit";
import { FailedToAutowire } from "../../../../src/2. Application/Errors/FailedToAutowire";
import { shouldRunSuite } from "../../../SuiteRunChecker";

if (shouldRunSuite('UNIT')) {
  describe('FailedToAutowire', () => {
    it('should be able to be made', () => {
      const className = randomStringGenerator(16)
      const methodName = randomStringGenerator(16)
      const error = new FailedToAutowire(className, methodName)
      expect(error).toBeInstanceOf(FailedToAutowire)
      expect(error.errorName).toBe(FailedToAutowire.name)
      expect(error.message).toBe(`Couldn't autowire '${className}.${methodName}'. Did you add '@AutoWire' to it?`)
    })
  })
}

if (shouldRunSuite('FUNCTIONAL')) {
  if (! shouldRunSuite('UNIT')) {
    describe('FailedToAutowire', () => {
      it('should be skipped because it\'s are not in the tests scope', () => {
        expect(true).toBe(true)
      })
    })
  }
}